# 
# Forecating procedure code.
# Author: Carlos Escuin
#

from optparse import OptionParser
import prediction
import utils


def forecast(numepoch, vconf, killing_bytes, init):
  """
  Main function of the forecasting procedure
  """

  if init:
    print('Generating endurance values (RW) for ' + vconf['suffix'] + ' ...')
    epoch = prediction.PredictionEpoch(-1, vconf)
    print('Cache effective capacity after generating endurance: ' + str(epoch.effectiveCapacity()))

  else:
    epoch = prediction.PredictionEpoch(numepoch, vconf)
    epoch.parseSimulationOutput()
    epoch.prediction(killing_bytes)
    epoch.dumpStats()
    print('Cache effective capacity after forecasting: ' + str(epoch.effectiveCapacity()))


def main():
  parser = OptionParser()
  parser.add_option("-v", "--version", type='choice', choices=['FD', 'FD+6', 'L2C2', 'L2C2+6'])
  parser.add_option("-n", "--numepoch", type='int')
  parser.add_option("-k", "--killing-bytes", type='int')
  parser.add_option("-s", "--suffix", type='string', default='')
  parser.add_option("-I", "--init", action='store_true', default=False)

  (options,args) = parser.parse_args()

  vconf = utils.load_config_json(options.version, options.suffix)

  forecast(options.numepoch, vconf, options.killing_bytes, options.init)


if __name__ == "__main__":
  main()
