# 
# Forecating procedure code.
# Author: Carlos Escuin
#

def parse(mix, numepoch, vconf):
  """
  Simulation output file parser to get simulated seconds (sim_seconds),
  IPC (ipc), the bytes written to NVM frames (bytes_written), and the
  LLC hit rate (hit_rate).
  The implemented parser is intended for gem5 using Ruby memory subsystem.
  PLEASE CHANGE THIS FUNCTION IF USING A DIFFERENT SIMULATOR.
  """

  bytes_written = [[[0 for j in range(vconf['ways'])] for i in range(vconf['sets'])] for h in range(vconf['num_llc_banks'])]
  simulation_output = vconf['simulation_output_dir'] + mix.replace("\n", "") + '_' + vconf['suffix'] + '_' + str(numepoch) + '.txt'
  f = open(simulation_output, 'r')

  insts = 0.0
  sim_seconds = 0.0
  llc_accesses = 0
  llc_hits = 0
  cycles = 0.0
  num_cores = vconf['num_cores']

  for line in f:
    if "sim_seconds" in line:
      sim_seconds = float(line.split()[1])
    elif "Instructions Simulated" in line:
      insts += float(line.split()[1])
    elif "numCycles" in line:
      if "switch_cpus" in line:
        cycles += float(line.split()[1])
    elif "NV-LLC.bytes_written" in line:
      if "total" in line:
        continue
      llc = int(line.split("l2_cntrl")[1].split(".")[0])
      i = int(line.split('written_')[1].split('::')[0])
      j = int(line.split('::')[1].split()[0])
      bytes_written[llc][i][j] = int(line.split()[1])
    elif "L2cache.demand_accesses" in line:
      llc_accesses += int(line.split()[1])
    elif "L2cache.demand_hits" in line:
      llc_hits += int(line.split()[1])
 
  cycles = cycles/num_cores
  ipc = float(insts)/cycles

  hit_rate = 0.0
  if llc_accesses > 0.0:
    hit_rate = float(llc_hits)*100/llc_accesses

  f.close()

  return sim_seconds, ipc, bytes_written, hit_rate

