# 
# Forecating procedure code.
# Author: Carlos Escuin
#

import llc
import simulation_parser
import heapq

class PredictionEpoch:
  def __init__(self, numepoch, vconf):
    self.numepoch = numepoch
    self.vconf = vconf
    self.llc_obj = llc.LLC(numepoch, vconf)
    self.wr_avg = {}
    self.distribution = {}
    self.ipc_total = 0.0
    self.hit_rate_total = 0.0
    self.epoch_duration = 0.0
    self.killed_bytes = 0


  def parseSimulationOutput(self):
    """
    Parse simulation output to obtain IPC and write rates
    and compute wr_avg for every tuple A
    """

    #Load LLC from byte map file
    self.llc_obj.loadLLC(self.numepoch)

    num_mixes = 0
    lines = open(self.vconf['workload_file'], 'r').readlines()
    print('*****Getting simulation outputs from epoch ' + str(self.numepoch) + '.*****')
    for line in lines:
      (sim_seconds, ipc, bytes_written, hit_rate) = simulation_parser.parse(line, self.numepoch, self.vconf)
      print('Simulated seconds for mix(' + line.replace("\n", "") + '): ' + str(sim_seconds))
      print('Aggregated IPC for mix(' + line.replace("\n", "") + '): ' + str(ipc))

      self.ipc_total += ipc
      self.hit_rate_total += hit_rate
      num_mixes += 1

      #Accumulate wr_avg from the bytes written to every frame
      for h in range(self.vconf['num_llc_banks']):
        for i in range(self.vconf['sets']):
          A = self.llc_obj.cacheSets[h][i].getA()
          if A not in self.distribution:
            self.wr_avg[A] = [0.0 for x in range(len(A))]
            self.distribution[A] = 0
          self.distribution[A] += 1
          for j in range(self.vconf['ways']):
            frame = self.llc_obj.cacheSets[h][i].frames[j]
            #Write rate per A tuple
            if frame.alive_bytes > 0:
              self.wr_avg[A][frame.getCC()] += bytes_written[h][i][j] / sim_seconds
    assert(num_mixes == self.vconf['nummixes'])

    #Average wr_avg for each A(tuple)
    for A in self.wr_avg:
      for i in range(len(A)):
        if A[i] > 0:
          self.wr_avg[A][i] = self.wr_avg[A][i] / (self.distribution[A] * A[i])
      self.distribution[A] = self.distribution[A] / num_mixes
    
    for A in self.wr_avg:
      print('\nA: ' + str(A) + ' n(A): ' + str(self.distribution[A]))
      print('wr_avg: ' + str([int(round(x)) for x in self.wr_avg[A]]) + '\n')

    self.ipc_total = self.ipc_total / num_mixes
    self.hit_rate_total = self.hit_rate_total / num_mixes


  def prediction(self, killing_bytes):
    """
    Prediction: compute the next N bytes that become faulty
    N = killing_bytes
    """

    plt = {} #Predicted lifetime

    #Compute plt from wr_avg and remaining writes.
    for h in range(self.vconf['num_llc_banks']):
      for i in range(self.vconf['sets']):
        cacheSet = self.llc_obj.cacheSets[h][i]
        for j in range(self.vconf['ways']):
          frame = cacheSet.frames[j]
          for k in range(len(frame.remaining_writes)):
            rw = frame.remaining_writes[k]
            if rw > 0.0 and self.wr_avg[cacheSet.getA()][frame.getCC()] > 0.0:
              #PLT(Bijk)
              dead_time = rw / (self.wr_avg[cacheSet.getA()][frame.getCC()] / frame.alive_bytes)
              if dead_time < cacheSet.next_dead_byte_time:
                cacheSet.next_dead_byte_time = dead_time
                cacheSet.next_dead_byte = (j, k)
        if cacheSet.next_dead_byte_time < float("inf"):
          plt[(h, i)] = cacheSet.next_dead_byte_time

    t = 0.0
    while self.killed_bytes < killing_bytes:
      if len(plt) == 0:
        break
      kb = min(plt, key=plt.get)
      assert(plt[kb] >= t)
      t = plt[kb]
      del plt[kb]

      cacheSet = self.llc_obj.cacheSets[kb[0]][kb[1]]
      print('Byte [' + str(kb[0]) + '] [' + str(kb[1]) + '] [' + str(cacheSet.next_dead_byte[0]) + '] [' + str(cacheSet.next_dead_byte[1]) + '] is becoming faulty. Dying time: ' + str(t))
      assert(t == cacheSet.next_dead_byte_time)
      killed_byte = cacheSet.killByte(self.wr_avg)

      if killed_byte == 1:
        self.killed_bytes += killed_byte

      if cacheSet.next_dead_byte_time < float("inf"):
        plt[(kb[0], kb[1])] = cacheSet.next_dead_byte_time

    #Update bytes remaining writes regarding wr_avg and epoch length
    self.epoch_duration = t
    for h in range(self.vconf['num_llc_banks']):
      for i in range(self.vconf['sets']):
        cacheSet = self.llc_obj.cacheSets[h][i]
        for j in range(self.vconf['ways']):
          frame = cacheSet.frames[j]
          for k in range(len(frame.remaining_writes)):
            if frame.remaining_writes[k] > 0.0:
              frame.remaining_writes[k] -= (self.epoch_duration - cacheSet.last_time_modified_state) * (self.wr_avg[cacheSet.getA()][frame.getCC()] / frame.alive_bytes)

    return self.killed_bytes


  #Dump extrapolation stats
  def dumpStats(self):
    self.llc_obj.dumpNextEpoch(self.numepoch)
    f = open(self.vconf['results_file'] + str(self.numepoch) + '.txt', 'w')

    assert(self.vconf['num_llc_banks']*self.vconf['sets'] == sum(self.distribution.values()))

    f.write('IPC: %.4f\n' % self.ipc_total)
    f.write('Hit rate: %.2f\n' % self.hit_rate_total)
    f.write('Alive bytes: ' + str(self.llc_obj.size*self.llc_obj.num_banks - self.llc_obj.byte_faulty_count) + '\n')
    f.write('Dead bytes: ' + str(self.llc_obj.byte_faulty_count) + '\n')
    f.write('Faulty frames: ' + str(self.llc_obj.frame_faulty_count) + '\n')
    f.write('Killed Bytes: ' + str(self.killed_bytes) + '\n')
    f.write('Epoch duration(s): ' + str(self.epoch_duration) + '\n')
    f.close()


  def effectiveCapacity(self):
    """
    Returns the LLC effective capacity (%)
    """
    if self.vconf['disabling'] == "FrameDisabling":
      return (1 - float((self.llc_obj.frame_faulty_count + self.killed_bytes) * self.llc_obj.blocksize) / (self.llc_obj.size * self.llc_obj.num_banks)) * 100
    else:
      return (1 - float(self.llc_obj.byte_faulty_count + self.killed_bytes) / (self.llc_obj.size * self.llc_obj.num_banks)) * 100

