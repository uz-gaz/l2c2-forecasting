# 
# Forecating procedure code.
# Author: Carlos Escuin
#

import numpy as np

def generateRemainingWrites(vconf):
  """
  Generate 'remaining_writes_file': All bitcells follow a normal 
  distribution of mean and cv specified in the config file.
  """
  real_blocksize = vconf['version_blocksize'] + vconf['spare_bytes']
  f = open(vconf['remaining_writes_file'] + '0.txt', 'w')

  #Please fix a seed for reproducibility
  seed = 12345678 #Change this, just a dummy value for the example
  np.random.seed(seed)

  values = np.random.normal(loc=vconf['normal_mean'], scale=vconf['normal_sd'], size=(vconf['num_llc_banks'], vconf['sets'], vconf['ways'], real_blocksize, 8))
  remaining_writes = [[[[0 for k in range(real_blocksize)] for j in range(vconf['ways'])] for i in range(vconf['sets'])] for h in range(vconf['num_llc_banks'])]
  
  for h in range(vconf['num_llc_banks']):
    for i in range(vconf['sets']):
      for j in range(vconf['ways']):
        for k in range(real_blocksize):
          remaining_writes[h][i][j][k] = float(min(values[h][i][j][k]))
          if remaining_writes[h][i][j][k] < 0.0:
            remaining_writes[h][i][j][k] = 0.0
          f.write('%.4f\n' % remaining_writes[h][i][j][k])
  
  print('Mean: ' + str(vconf['normal_mean']))
  print('STD: ' + str(vconf['normal_sd']))
  f.close()

  return remaining_writes

def loadRemainingWrites(numepoch, vconf):
  real_blocksize = vconf['version_blocksize'] + vconf['spare_bytes']
  lines = open(vconf['remaining_writes_file'] + str(numepoch) + '.txt', 'r').readlines()
  remaining_writes = [[[[0.0 for k in range(real_blocksize)] for j in range(vconf['ways'])] for i in range(vconf['sets'])] for h in range(vconf['num_llc_banks'])]
  
  for h in range(vconf['num_llc_banks']):
    for i in range(vconf['sets']):
      for j in range(vconf['ways']):
        for k in range(real_blocksize):
          remaining_writes[h][i][j][k] = float(lines[h*vconf['sets']*vconf['ways']*real_blocksize + i*vconf['ways']*real_blocksize + j*real_blocksize + k].replace("\n", ""))

  return remaining_writes

def dump(values, epochnum, vconf):
  real_blocksize = vconf['version_blocksize'] + vconf['spare_bytes']
  f = open(vconf['remaining_writes_file'] + str(epochnum) + '.txt', 'w')
  
  for h in range(vconf['num_llc_banks']):
    for i in range(vconf['sets']):
      for j in range(vconf['ways']):
        for k in range(real_blocksize):
          f.write('%.4f\n' % values[h][i][j][k])
    
  f.close()

