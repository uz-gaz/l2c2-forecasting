# 
# Forecating procedure code.
# Author: Carlos Escuin
#

import json

CONFIG_FILE = 'config.json'

def load_config_json(version, suffix=''):
  """
  Load configuration file into vconf dictionary
  """

  vconf = {}

  with open(CONFIG_FILE) as versions_json:
    versions_config = json.load(versions_json)

  versions_config['general']['normal_mean'] = 10 ** versions_config['general']['normal_mean']

  versions_config['general']['normal_sd'] = versions_config['general']['normal_mean'] * versions_config['general']['normal_cov']

  versions_config['general']['sets'] = versions_config['general']['bank_size'] / versions_config['general']['blocksize'] / versions_config['general']['ways']

  versions_config['general']['nummixes'] = len(open(versions_config['general']['workload_file'], 'r').readlines())

  #Add suffix
  if suffix != '':
    versions_config[version]['suffix'] = versions_config[version]['suffix'] + suffix
    versions_config[version]['fault_map_file'] = versions_config[version]['fault_map_file'].split(version)[0] + version + suffix + versions_config[version]['fault_map_file'].split(version)[1]
    versions_config[version]['ecps_file'] = versions_config[version]['ecps_file'].split(version)[0] + version + suffix + versions_config[version]['ecps_file'].split(version)[1]
    versions_config[version]['remaining_writes_file'] = versions_config[version]['remaining_writes_file'].split(version)[0] + version + suffix + versions_config[version]['remaining_writes_file'].split(version)[1]
    versions_config[version]['results_file'] = versions_config[version]['results_file'].split(version)[0] + version + suffix + versions_config[version]['results_file'].split(version)[1]

  for key in versions_config['general']:
    vconf[key] = versions_config['general'][key]

  for key in versions_config[version]:
    vconf[key] = versions_config[version][key]

  return vconf
