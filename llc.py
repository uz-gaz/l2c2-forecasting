# 
# Forecating procedure code.
# Author: Carlos Escuin
#

import endurance_generator


class Frame:
  def __init__(self, vconf, alive_bytes, remaining_writes, alive_ecps):
    self.vconf = vconf
    self.alive_bytes = alive_bytes
    self.alive_ecps = alive_ecps
    self.remaining_writes = remaining_writes
    self.CEs = vconf['CEs']
    self.CC = self.computeCC()
    self.unconfirmedCC = self.CC

  def getCC(self):
    return self.CC

  def getUnconfirmedCC(self):
    return self.unconfirmedCC

  def killByte(self, k):
    killed_byte = 0
    self.remaining_writes[k] = 0.0
    if self.alive_ecps > 0:
      self.alive_ecps -= 1
    else:
      self.alive_bytes -= 1
      if self.alive_bytes < self.vconf['version_blocksize']:
        killed_byte = 1
        self.unconfirmedCC = self.computeCC()

    return killed_byte

  def computeCC(self):
    for i in range(len(self.CEs)):
      if self.alive_bytes == self.CEs[i]:
        return i
      elif self.alive_bytes < self.CEs[i]:
        return i - 1
    return i

  def confirmCC(self):
    self.CC = self.unconfirmedCC
    

class CacheSet:
  def __init__(self, llc, CEs, assoc, frames):
    self.llc = llc
    self.CEs = CEs
    self.ways = assoc
    self.frames = frames
    self.last_time_modified_state = 0.0
    self.next_dead_byte = (None, None)
    self.next_dead_byte_time = float("inf")
    assert(len(frames) == self.ways)
    self.A = self.computeA()
    self.unconfirmedA = self.A
    self.changedA = False

  def getA(self):
    return tuple(self.A)

  def getUnconfirmedA(self):
    return tuple(self.unconfirmedA)

  def confirmA(self):
    self.A = self.unconfirmedA
    for f in self.frames:
      f.confirmCC()
    self.changedA = True

  def computeA(self):
    A = [0 for i in range(len(self.CEs))]
    for f in self.frames:
      A[f.getUnconfirmedCC()] += 1
    return A

  def updateRemainingWrites(self, t, wr_avg):
    for j in range(self.ways):
      frame = self.frames[j]
      for k in range(len(frame.remaining_writes)):
        if frame.remaining_writes[k] > 0.0:
          frame.remaining_writes[k] -= (t - self.last_time_modified_state) * (wr_avg[self.getA()][frame.getCC()] / frame.alive_bytes)
    self.last_time_modified_state = t

  def killByte(self, wr_avg):
    existing_A = 0
    new_A = 0
    c_j = self.next_dead_byte[0]
    b_k = self.next_dead_byte[1]
    t = self.next_dead_byte_time
    self.next_dead_byte = (None, None)
    self.next_dead_byte_time = float("inf")
    
    self.updateRemainingWrites(t, wr_avg)

    killed_byte = self.frames[c_j].killByte(b_k)
    self.unconfirmedA = self.computeA()
    if self.frames[c_j].alive_bytes == 0:
      self.llc.alive_frames -= 1

    #New A appeared and we have data (present in wr_avg)
    if self.getUnconfirmedA() in wr_avg and self.getUnconfirmedA() != self.getA():
      print('Set changing to A: ' + str(self.getUnconfirmedA()))
      self.confirmA()

    #New A appeared but no data (not present in wr_avg)
    elif self.getUnconfirmedA() not in wr_avg:
      self.changedA = False
      if self.getA()[self.frames[c_j].getUnconfirmedCC()] > 0 or self.llc.vconf['disabling'] == "FrameDisabling":
        self.frames[c_j].confirmCC()
      print('New A appeared: ' + str(self.getUnconfirmedA()))

    #Set remains in the same A
    else:
      assert(self.getA() == self.getUnconfirmedA())
      print('CacheSet remains in A: ' + str(self.getA()))

    #Update bytes dying time
    for j in range(self.ways):
      frame = self.frames[j]
      for k in range(len(frame.remaining_writes)):
        if frame.remaining_writes[k] > 0.0:
          dead_time = float("inf")
          if wr_avg[self.getA()][frame.getCC()] > 0.0:
            dead_time = self.last_time_modified_state + frame.remaining_writes[k] / (wr_avg[self.getA()][frame.getCC()] / frame.alive_bytes)
          if dead_time < self.next_dead_byte_time:
            self.next_dead_byte_time = dead_time
            self.next_dead_byte = (j, k)

    return killed_byte


class LLC:
  def __init__(self, numepoch, vconf):
    self.cacheSets = [[None for i in range(vconf['sets'])] for h in range(vconf['num_llc_banks'])]
    self.vconf = vconf
    self.num_banks = vconf['num_llc_banks']
    self.num_sets = vconf['sets']
    self.num_ways = vconf['ways']
    self.blocksize = vconf['version_blocksize']
    self.size = self.num_sets * self.num_ways * self.blocksize
    self.byte_faulty_count = 0
    self.frame_faulty_count = 0
    self.alive_frames = 0
    self.ecps = vconf['ecps']
    self.spare_bytes = vconf['spare_bytes']
    if numepoch == -1:
      self.generateLLC()
    
  def generateLLC(self):
    self.byte_faulty_count = 0
    self.frame_faulty_count = 0
    self.alive_frames = 0
    rw_values = endurance_generator.generateRemainingWrites(self.vconf)
    fault_map_file = self.vconf['fault_map_file'] + '0_'
    ecps_file = self.vconf['ecps_file'] + '0_'
    
    f2 = None
    for h in range(self.num_banks):
      f = open(fault_map_file + str(h) + '.txt', 'w')
      if self.ecps > 0:
        f2 = open(ecps_file + str(h) + '.txt', 'w')
      for i in range(self.num_sets):
        frames = [None for j in range(self.num_ways)]
        for j in range(self.num_ways):
          alive_bytes = self.blocksize+self.spare_bytes
          alive_ecps = self.ecps
          for k in range(self.blocksize+self.spare_bytes):
            if rw_values[h][i][j][k] == 0.0:
              if alive_ecps > 0:
                alive_ecps -= 1
              else:
                alive_bytes -= 1
                if alive_bytes < self.blocksize:
                  self.byte_faulty_count += 1
          f.write(str(alive_bytes) + '\n')
          if self.ecps > 0:
            f2.write(str(alive_ecps) + '\n')
          frames[j] = Frame(self.vconf, alive_bytes, rw_values[h][i][j], alive_ecps)
          if alive_bytes < self.blocksize:
            self.frame_faulty_count += 1
          if alive_bytes > 0:
            self.alive_frames += 1
        self.cacheSets[h][i] = CacheSet(self, self.vconf['CEs'], self.num_ways, frames)
      f.close()
      if f2 != None:
        f2.close()
  
    print('LLC byte map generated. Size ' + str(self.num_banks*self.size) + 'B.')
    print(str(self.byte_faulty_count) + ' faulty bytes out of ' + str(self.num_banks*self.size) + '. ' + str(float(self.byte_faulty_count)*100/self.num_banks/self.size) + ' %.')
    print(str(self.frame_faulty_count) + ' faulty frames out of ' + str(self.num_ways*self.num_sets*self.num_banks) + '. ' + str(float(self.frame_faulty_count)*100/self.num_sets/self.num_banks/self.num_ways) + ' %.')
  
  def loadLLC(self, numepoch):
    self.byte_faulty_count = 0
    self.frame_faulty_count = 0
    self.alive_frames = 0
    rw_values = endurance_generator.loadRemainingWrites(numepoch, self.vconf)
    fault_map_file = self.vconf['fault_map_file'] + str(numepoch) + '_'
    ecps_file = self.vconf['ecps_file'] + str(numepoch) + '_'
    
    lines_ecps = None
    for h in range(self.num_banks):
      lines = open(fault_map_file + str(h) + '.txt', 'r').readlines()
      if self.ecps > 0:
        lines_ecps = open(ecps_file + str(h) + '.txt', 'r').readlines()
      for i in range(self.num_sets):
        frames = [None for j in range(self.num_ways)]
        for j in range(self.num_ways):
          alive_bytes = int(lines[i*self.num_ways + j].replace("\n", ""))
          alive_ecps = 0
          if self.ecps > 0:
            alive_ecps = int(lines_ecps[i*self.num_ways + j].replace("\n", ""))
          frames[j] = Frame(self.vconf, alive_bytes, rw_values[h][i][j], alive_ecps)
          if alive_bytes < self.blocksize:
            self.byte_faulty_count += self.blocksize - alive_bytes
            self.frame_faulty_count += 1
          if alive_bytes > 0:
            self.alive_frames += 1
        self.cacheSets[h][i] = CacheSet(self, self.vconf['CEs'], self.num_ways, frames)
  
  def dumpNextEpoch(self, numepoch):
    rw_values = [[[[ 0 for k in range(self.blocksize)] for j in range(self.num_ways)] for i in range(self.num_sets)] for h in range(self.num_banks)]
    fault_map_file = self.vconf['fault_map_file'] + str(numepoch+1) + '_'
    ecps_file = self.vconf['ecps_file'] + str(numepoch+1) + '_'
    
    byte_faulty_count = 0
    frame_faulty_count = 0
    f2 = None
    for h in range(self.num_banks):
      f = open(fault_map_file + str(h) + '.txt', 'w')
      if self.ecps > 0:
        f2 = open(ecps_file + str(h) + '.txt', 'w')
      for i in range(self.num_sets):
        for j in range(self.num_ways):
          alive_bytes = self.cacheSets[h][i].frames[j].alive_bytes
          alive_ecps = self.cacheSets[h][i].frames[j].alive_ecps
          f.write(str(alive_bytes) + '\n')
          if self.ecps > 0:
            f2.write(str(alive_ecps) + '\n')
          rw_values[h][i][j] = self.cacheSets[h][i].frames[j].remaining_writes
          if alive_bytes < self.blocksize:
            byte_faulty_count += self.blocksize - alive_bytes
            frame_faulty_count += 1
      f.close()
      if f2 != None:
        f2.close()
 
    endurance_generator.dump(rw_values, numepoch+1, self.vconf)

    print('')
    print('LLC byte map dumped for next epoch.')
    print(str(byte_faulty_count) + ' faulty bytes out of ' + str(self.num_banks*self.size) + '. ' + str(float(byte_faulty_count)*100/self.num_banks/self.size) + ' %.')
    print(str(frame_faulty_count) + ' faulty frames out of ' + str(self.num_ways*self.num_sets*self.num_banks) + '. ' + str(float(frame_faulty_count)*100/self.num_sets/self.num_banks/self.num_ways) + ' %.')
    print('')
  
